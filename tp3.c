#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>


#include "mt19937ar.c"

/*-------------------------------------------------------------------------*/
/*----------------------------- Structures --------------------------------*/
/*-------------------------------------------------------------------------*/

/* Structure contenant les paramètres d'une simulation de la valeur de pi */
struct ThreadData {
    int threadID;
    double result;
    int numPoints;
};

/*-------------------------------------------------------------------------*/
/*----------------------------- Fonctions ---------------------------------*/
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* simPi : Simule la valeur de pi en utilisant la méthode de Monte Carlo   */
/* Entrée :                                                                */
/*      numPoints : Nombre de points à utiliser pour la simulation         */
/* Sortie :                                                                */
/*      pi : Valeur de pi calculée par la simulation                       */
/*-------------------------------------------------------------------------*/
double simPi(int numPoints){
    int i, count = 0;
    double x, y, pi;
    for(i = 0; i < numPoints; i++){
        x = genrand_real2(); //genrand_real2() retourne un nombre aléatoire entre 0 et 1
        y = genrand_real2();
        if(x*x + y*y <= 1){
            count++; //Si le point est dans le cercle, on incrémente le compteur
        }
    }
    pi = 4.0 * count / numPoints; //On calcule la valeur de pi
   // printf("PI = %lf\n", pi);
    return pi;
}

/*-------------------------------------------------------------------------*/
/* threadSimPi : Fonction exécutée par chaque thread pour simuler la valeur*/
/*               de pi                                                     */
/* Entrée :                                                                */
/*      arg : Pointeur vers une structure ThreadData contenant les         */
/*            paramètres de la simulation                                  */
/*-------------------------------------------------------------------------*/
void *threadSimPi(void *arg) {
    struct ThreadData *data = (struct ThreadData *)arg;
    data->result = simPi(data->numPoints);
    pthread_exit(NULL);
}

/*-------------------------------------------------------------------------*/
/* repeatSimPi : Répète la simulation de la valeur de pi plusieurs fois    */
/* Entrée :                                                                */
/*      n : Nombre de fois que la simulation doit être répétée             */
/*      nbPoints : Nombre de points à utiliser pour la simulation          */
/* Sortie :                                                                */
/*      meanPi : Moyenne des valeurs de pi calculées par la simulation     */
/*-------------------------------------------------------------------------*/
double repeatSimPi(int n, int nbPoints,double* array){
    double sum = 0;
    for(int i = 0; i < n; i++){
        array[i] += simPi(nbPoints); 
        sum += array[i];
    }
    double meanPI = sum / (double)n; //On calcule la moyenne des valeurs de pi
    printf("Moyenne de PI : %lf\n", meanPI); 
    printf("Erreur Absolue : %lf\n", fabs(meanPI - M_PI)); 
    printf("Erreur Relative : %lf\n", fabs(meanPI - M_PI) / M_PI);
    
    return meanPI;

}

/*-------------------------------------------------------------------------*/
/* repeatSimPiMultiThreaded : Répète la simulation de la valeur de pi      */
/*                            plusieurs fois en utilisant plusieurs fils   */
/*                            d'exécution                                  */
/* Entrée :                                                                */
/*      n : Nombre de fois que la simulation doit être répétée             */
/*      nbPoints : Nombre de points à utiliser pour la simulation          */
/* Sortie :                                                                */
/*      meanPi : Moyenne des valeurs de pi calculées par la simulation     */
/*-------------------------------------------------------------------------*/
void repeatSimPiMultiThreaded(int n, int nbPoints){
 

    

    pthread_t threads[n]; //On crée un tableau de threads
    
    struct ThreadData results[n]; //On crée un tableau de structures ThreadData

    for (int i = 0; i < n; i++) {
        results[i].threadID = i; 
        results[i].numPoints = nbPoints;
        pthread_create(&threads[i], NULL, threadSimPi, (void *)&results[i]); //On crée un thread pour chaque simulation
    }

    for (int i = 0; i < n; i++) {
        pthread_join(threads[i], NULL); //On attend que tous les threads aient terminé
        printf("Thread %d: PI = %lf\n", results[i].threadID, results[i].result);
    }
}

/*-------------------------------------------------------------------------*/
/* confidenceInterval : Calcule l'intervalle de confiance pour la valeur   */
/*                      de pi                                              */
/* Entrée :                                                                */
/*      alpha : Niveau de confiance                                        */
/* Sortie :                                                                */
/*      interval : Intervalle de confiance pour la valeur de pi            */
/*-------------------------------------------------------------------------*/
void confidenceInterval(){

    int nbSimulations = 1000000; //Nombre de simulations

    //Tableaux des valeurs de t pour alpha = 0.05 et alpha = 0.01
    double student95[41] = {  0,12.706,4.303,3.182,2.776,2.571,2.447,2.365,2.306,2.262,
                            2.228,2.201,2.179,2.160,2.145,2.131,2.120,2.110,2.101,2.093,
                            2.086,2.080,2.074,2.069,2.064,2.060,2.056,2.052,2.048,2.045,
                            2.042,2.040,2.037,2.035,2.032,2.030,2.028,2.026,2.024,2.023,2.021};

    double student99[41] = {  0,63.657,9.925,5.841,4.604,4.032,3.707,3.499,3.355,3.250,
                            3.169,3.106,3.055,3.012,2.977,2.947,2.921,2.898,2.878,2.861,
                            2.845,2.831,2.819,2.807,2.797,2.787,2.779,2.771,2.763,2.756,
                            2.750,2.744,2.738,2.733,2.728,2.724,2.719,2.715,2.712,2.708,2.704};


    printf("Entrez le nombre de simulations : \n");
    int numSimulations;
    scanf("%d", &numSimulations);

    double sum = 0;
    double * array = calloc(numSimulations,sizeof(double)); 
    double meanPi = repeatSimPi(numSimulations, nbSimulations,array); //On calcule la moyenne des valeurs de pi
    for(int i = 0; i< numSimulations; i++){ 
        sum+= pow(array[i]-meanPi,2);
    }

    double variance2 = sum / (numSimulations - 1);

    //On calcule les rayons des intervalles de confiance    
    double R_95 = student95[numSimulations+1] * sqrt(variance2 / numSimulations); 
    double R_99 = student99[numSimulations+1] * sqrt(variance2 / numSimulations);

    double variance  = sqrt(variance2);
    printf("Variance : %lf\n", variance);
    double erreurstandard = variance / sqrt(numSimulations);
    printf("Erreur standard : %lf\n", erreurstandard);

    printf("Intervalle de confiance avec alpha = 0.05 : [%lf, %lf]\n", meanPi - R_95, meanPi + R_95);
    printf("Intervalle de confiance avec alpha = 0.01 : [%lf, %lf]\n", meanPi - R_99, meanPi + R_99);
    
}

void test_SimPi(){
    int numPoints[] = {1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000, 10000000, 100000000, 1000000000};

     for (int i = 0; i < 13; i++) {
        printf("Exécution à %d points :\n", numPoints[i]);
        double pi = simPi(numPoints[i]);
        printf("PI = %lf\n", pi);
    }

}

void test_repeatSimP(){
    for(int i = 10; i <=40; i+=10){
         double array[40] = {0};
        repeatSimPi(i, 1000,array);
    }
}

int main(int argc, char const *argv[])
{
    //Initialisation du Mersenne Twister
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);
   


    if (argc == 2) {
        if (strcmp(argv[1], "simPi") == 0) {
            test_SimPi();
        } else if (strcmp(argv[1], "repeatSimPi") == 0) {
            test_repeatSimP();
        } else if (strcmp(argv[1], "repeatSimPiMultiThreaded") == 0) {
            repeatSimPiMultiThreaded(10, 1000000);
        } else if (strcmp(argv[1], "confidenceInterval") == 0) {
            confidenceInterval();
        } else {
            printf("Argument invalide\n");
        }
    } else {
        printf("Usage: %s [simPi | repeatSimPi | repeatSimPiMultiThreaded | confidenceInterval]\n", argv[0]);
    }
   



     

    return 0;
}
